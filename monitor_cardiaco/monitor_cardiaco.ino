//configuracion de sensor de temperatura y humedad
#include "DHT.h"
#define DHTTYPE DHT11
#define dht_dpin 0
DHT dht(dht_dpin, DHTTYPE);
//fin configuracion sensor de temperatura y humedad

#define USE_ARDUINO_INTERRUPTS true    // Set-up low-level interrupts for most acurate BPM math.
#include <PulseSensorPlayground.h>     // Includes the PulseSensorPlayground Library.   

//wifi
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
//fin wiwi

const char* ssid = "'''''''''''";
const char* password = "'''''''"; //borrar antes de subir

//url del servicio de backend
const char* server = "http://192.168.0.112:3000/setInfo";


bool DEBUG_ACTIVO = true;

unsigned long last_time = 0;
unsigned long timer_delay = 20000;
WiFiClient wifiClient;


int pausa = 1000;
const int puerto = 9600;


int cardio = 0;

//variables que se enviaran en el cuerpo del JSON
String temperatura;
String humedad;
String formatoTemperatura;



String pulsaciones;
int umbralMuestras = 100;
int pinSensorCardio = 0;
String arrayPulsaciones = "";
int muestras = 0;

  //sensor cardiaco
  //  Variables
  const int PulseWire = 0;       // PulseSensor PURPLE WIRE connected to ANALOG PIN 0
  const int LED13 = 13;          // The on-board Arduino LED, close to PIN 13.
  int Threshold = 550;           // Determine which Signal to "count as a beat" and which to ignore.
                               // Use the "Gettting Started Project" to fine-tune Threshold Value beyond default setting.
                               // Otherwise leave the default "550" value. 
                               
PulseSensorPlayground pulseSensor;  // Creates an instance of the PulseSensorPlayground object called "pulseSensor"



void setup(void)
{
  dht.begin();
  Serial.begin(puerto);
  pinMode(5, INPUT);

  //setup wifi
  WiFi.begin(ssid, password);
  Serial.println("Conectando a red WIFI…");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());



  // Configure the PulseSensor object, by assigning our variables to it. 
  pulseSensor.analogInput(PulseWire);   
  pulseSensor.blinkOnPulse(LED13);       //auto-magically blink Arduino's LED with heartbeat.
  pulseSensor.setThreshold(Threshold);   

  // Double-check the "pulseSensor" object was created and "began" seeing a signal. 
   if (pulseSensor.begin()) {
    Serial.println("We created a pulseSensor Object !");  //This prints one time at Arduino power-up,  or on Arduino reset.  
  }

}
void loop() {
  if(muestras > umbralMuestras){
    obtenerTemperaturaHumedad();
    enviarInfo();
    muestras = 0;
    arrayPulsaciones= "";
  }

  capturarPulsaciones();
  muestras++;
  
}

void capturarPulsaciones(){
  int inputVal = analogRead (pinSensorCardio);
  if(inputVal < Threshold) inputVal = Threshold;
  
  arrayPulsaciones = arrayPulsaciones +   inputVal + ",";
  delay(10);
}



void obtenerTemperaturaHumedad() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  humedad = String(h, 3);
  temperatura = String(t, 3);
  if (DEBUG_ACTIVO) {
    Serial.print("humedad = ");
    Serial.print(h);
    Serial.print("% ");
    Serial.print("temperatura = ");
    Serial.print(t);
    Serial.println("C ");
  }
}




void enviarInfo() {
  //Send an HTTP POST request every 10 seconds
  //if ((millis() - last_time) > timer_delay) {

  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;


    http.begin(wifiClient, server);

    http.addHeader("Content-Type", "application/json");
    //armado del json que se enviara
    String httpRequestData =  String("{\"temperatura\":\"") + temperatura + "\",\"humedad\":\"" + humedad + "\"" + ",\"cardio\":\"" + arrayPulsaciones + "\"" + "}";

    int httpResponseCode = http.POST(httpRequestData);
    if (DEBUG_ACTIVO) {
      //Serial.print(httpRequestData);
      Serial.print("HTTP Response: ");
      Serial.println(httpResponseCode);
    }
    http.end();
  }
  else {
    Serial.println("WiFi no conectado");
  }
  //last_time = millis();
  //}
}
